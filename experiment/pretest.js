### Pre Test <br>
myQuestions.forEach((currentQuestion, questionNumber) => {
    // we'll want to store the list of answer choices
    const answers = [];

    // and for each available answer...
    for (letter in currentQuestion.answers) {
      // ...add an HTML radio button
      answers.push(
        `<label>
          <input type="radio" name="question${questionNumber}" value="${letter}">
          ${letter} :
          ${currentQuestion.answers[letter]}
        </label>`
      );
    }

    // add this question and its answers to the output
    output.push(
      `<div class="question"> ${currentQuestion.question} </div>
      <div class="answers"> ${answers.join("")} </div>`
    );
  });

  // finally combine our output list into one string of HTML and put it on the page
  quizContainer.innerHTML = output.join("");
}

function showResults() {
  // gather answer containers from our quiz
  const answerContainers = quizContainer.querySelectorAll(".answers");

  // keep track of user's answers
  let numCorrect = 0;

  // for each question...
  myQuestions.forEach((currentQuestion, questionNumber) => {
    // find selected answer
    const answerContainer = answerContainers[questionNumber];
    const selector = `input[name=question${questionNumber}]:checked`;
    const userAnswer = (answerContainer.querySelector(selector) || {}).value;

    // if answer is correct
    if (userAnswer === currentQuestion.correctAnswer) {
      // add to the number of correct answers
      numCorrect++;

      // color the answers green
      //answerContainers[questionNumber].style.color = "lightgreen";
    } else {
      // if answer is wrong or blank
      // color the answers red
      answerContainers[questionNumber].style.color = "red";
    }
  });

  // show number of correct answers out of total
  resultsContainer.innerHTML = `${numCorrect} out of ${myQuestions.length}`;
}

const quizContainer = document.getElementById("quiz");
const resultsContainer = document.getElementById("results");
const submitButton = document.getElementById("submit");







/////////////// Write the MCQ below in the exactly same described format ///////////////


const myQuestions = [
  {
    question: "Q.1 Find the Output of the Following? "<br>
    "number1 <-10"<br>
	"number2 <-8"<br>
    "print(sum(number1,number2))",  ///// Write the question inside double quotes
    
    answers: {
     a:"10,8",
     b:"sum(10,8)",
     c:"18",
     d:"Error" 
    },
    correctAnswer:"c"              ///// Write the correct option inside double quotes
  },

  {
   question: "Find the Out put of the Following" <br>
    " number1 <-10"<br>
   "number2 <-4"<br>
   "print(number1 %/% number2)",  ///// Write the question inside double quotes
    answers: {
      a: "10,8",                  ///// Write the option 1 inside double quotes
      b: "2",                  ///// Write the option 2 inside double quotes
      c: "2.5",                  ///// Write the option 3 inside double quotes
      d: "Error"                   ///// Write the option 4 inside double quotes
    },
    correctAnswer: "b"                ///// Write the correct option inside double quotes
  },                                  ///// To add more questions, copy the section below 
                                                        ///// this line
 {
    question: "Find the Out put of the Following"<br>
    "number1 <-10"<br>
	"number2 <-4"<br>
	"print(number1 % number2)",
    answers: {
      a: "10,8",
      b: "2",                  
      c: "2.5",                 
      d: "Error"  
    },
    correctAnswer: "d"
  },

];




/////////////////////////////////////////////////////////////////////////////

/////////////////////// Do not modify the below code ////////////////////////

/////////////////////////////////////////////////////////////////////////////


// display quiz right away
buildQuiz();

// on submit, show results
submitButton.addEventListener("click", showResults);
})();
