### Theory
 <h3></h3>
                   An operator is an indicator, a symbol that shows that some specific operation needs to be performed within a computer program. These operators can be arithmetic, logical, or relational, and mimic what we see in the real-world. If you think back to grade school, you will remember them as the rules of addition and subtraction, and even those many multiplication tables you needed to memorize! It works much the same way with computers. We ask them to perform various formula-based calculations in the programs we write. An arithmetic operator in programming is a subset of these indicators or symbols that denote that a specific mathematical operation is needed. You can think of them much like their real-world equivalents. They form part of a program statement that determines the result required. For example, you would use one or more operators to calculate the total dollars you are paid in your paycheck or the total amount you owe when you purchase an item. Operators impact many aspects of our lives, although we are often unaware of them. They do their thing quietly, behind the scenes, as part of the computers we employ.
                    <br><br>
                    The below mentioned table gives the arithmetic operators hold up by R language. The operators act on each element of the vector.<br><br>
Addition 
g <- c (4, 6.5, 6)

s <- c (8, 3, 5)

print (g + s)

Subtraction 
g <- c ( 2, 5.5, 6)

s <- c (8, 3, 4)

print (g - s)

Multiplication 
g <- c ( 26.5,8)

s <- c(6, 4, 3)

print (g * s)

Division 
g <- c( 2,4.6,8)

s <- c(8, 4, 3)

print (g / s)
Modulus 
g <- c ( 2, 5.5, 8)

s <- c (8, 4, 5)

print (g% / %s)
                    <h3><u>IT WILL HELP IN :</h3></u>
                    * Basic addition/subtraction facts.<br>
                    * Mental addition and subtraction (for example, whole hundreds).<br>
                    * Multiplication tables you can make for the calculation purpose.<br>
                    * Basic division facts, including with remainders.<br>
                    * Mental multiplication and division (for example, by powers of ten).<br>
                    

<h4>Steps of simulator :</h4><br>
To understand the functioning of simulator as it displayed 3 sections : Initialization,Execution and Output.<br>
1. Select the operator.<br>
2. Enter the two operands which you want. <br>
3. Press the start button. <br>
4. Now, how execution takes place will be displayed on the execution section. <br>
5. Click on Next ,till your execution end and output displayed. <br>
6. Now to reuse the simulator; click on reset button, it will reset yor simulator back to start.<br>
