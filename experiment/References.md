### References
<p style="font-size:100%; margin-top:2%">
                        1.&nbsp;Handbook of programming with R by Garrett Grolemund.
                        <br><br>
                        2.&nbsp;The Art of R programming by Norman Matloff.
                        <br><br>
                        3.&nbsp;R packages by Hadley Wickham.
                        <br><br>

<h3>Webliography :</h3>
                        <br>
                        1.&nbsp;https://www.w3schools.in/r/operators/
                        <br>
                        2.&nbsp;https://www.geeksforgeeks.org/operators-in-c-set-1-arithmetic-operators/
                        <br>
                        3.&nbsp;https://www.tutorialspoint.com/r/r_operators.htm
                        <br><br>
                        <h3>Additional video links :</h3>
                        <br>
                        1.&nbsp;https://www.youtube.com/watch?v=UbSZ89Ze5lw
                        <br>
                        2.&nbsp;https://www.youtube.com/watch?v=0m1rfh4qSyc
                  <!--Theory of experiment -->
                    </p>
                        